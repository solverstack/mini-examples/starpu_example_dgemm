
#include <starpu.h>
#include <starpu_mpi.h>
#include <mpi.h>
#include "dsmat.h"
#include "optional_tile_interface.h"

static inline Block *
ti_handle_get( starpu_data_handle_t handle )
{
    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node( handle, STARPU_MAIN_RAM );

#ifdef STARPU_DEBUG
    STARPU_ASSERT_MSG( tile_interface->id == starpu_interface_tile_ops.interfaceid,
                       "Error. The given data is not a _tile." );
#endif

    return &(tile_interface->tile);
}

static void
ti_init( void *data_interface )
{
    starpu_tile_interface_t *tile_interface = data_interface;
    tile_interface->id = starpu_interface_tile_ops.interfaceid;
    tile_interface->allocsize = -1;
}

static void
ti_register_data_handle( starpu_data_handle_t  handle,
                          unsigned              home_node,
                          void                 *data_interface )
{
    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *) data_interface;
    unsigned node;

    for (node = 0; node < STARPU_MAXNODES; node++)
    {
        starpu_tile_interface_t *local_interface = (starpu_tile_interface_t *)
            starpu_data_get_interface_on_node(handle, node);

        memcpy( local_interface, tile_interface,
                sizeof( starpu_tile_interface_t ) );

        if ( node != home_node )
        {
            local_interface->dev_handle = 0;
            local_interface->tile.c  = NULL;
            local_interface->tile.ld   = -1;
        }
    }
}

static starpu_ssize_t
ti_allocate_data_on_node( void *data_interface, unsigned node )
{
    uintptr_t addr = 0, handle;
    starpu_tile_interface_t *tile_interface =
        (starpu_tile_interface_t *) data_interface;

    uint32_t ld = tile_interface->tile.m;
    starpu_ssize_t allocated_memory;

    allocated_memory = tile_interface->allocsize;
    if ( allocated_memory <= 0 ) {
        return 0;
    }

    handle = starpu_malloc_on_node( node, allocated_memory );

    if ( !handle ) {
        return -ENOMEM;
    }

    if ( starpu_node_get_kind(node) != STARPU_OPENCL_RAM ) {
        addr = handle;
    }

    /* update the data properly */
    tile_interface->tile.c   = (void*)addr;
    tile_interface->tile.ld    = ld;
    tile_interface->dev_handle = handle;

    return allocated_memory;
}

static void
ti_free_data_on_node( void *data_interface, unsigned node )
{
    starpu_tile_interface_t *tile_interface =
        (starpu_tile_interface_t *) data_interface;

    {
        assert( (uintptr_t)(tile_interface->tile.c) == tile_interface->dev_handle );
    }

    starpu_free_on_node( node, tile_interface->dev_handle, tile_interface->allocsize );
    tile_interface->tile.c = NULL;
    tile_interface->dev_handle = 0;
}

static void *
ti_to_pointer( void *data_interface, unsigned node )
{
    (void) node;
    starpu_tile_interface_t *tile_interface = data_interface;

    return (void*)(tile_interface->tile.c);
}

static int
ti_pointer_is_inside( void *data_interface, unsigned node, void *ptr )
{
    (void) node;
    starpu_tile_interface_t *tile_interface = data_interface;
    char *begin = (char*) tile_interface->tile.c;
    char *end   = begin + tile_interface->allocsize;


    return ( (char*) ptr >= begin )
        && ( (char*) ptr <  end   );
}

static size_t
ti_get_size(starpu_data_handle_t handle)
{
    starpu_tile_interface_t *tile_interface =
        starpu_data_get_interface_on_node( handle, STARPU_MAIN_RAM );
    size_t elemsize = sizeof(double);

    return tile_interface->tile.m * tile_interface->tile.n * elemsize;
}

static size_t
ti_get_alloc_size(starpu_data_handle_t handle)
{
    starpu_tile_interface_t *tile_interface =
        starpu_data_get_interface_on_node( handle, STARPU_MAIN_RAM );

    STARPU_ASSERT_MSG( tile_interface->allocsize != (size_t)-1,
                       "The _tile allocation size needs to be defined" );

    return tile_interface->allocsize;
}

static uint32_t
ti_footprint( starpu_data_handle_t handle )
{
    Block *tile = ti_handle_get( handle );
    return starpu_hash_crc32c_be( tile->m, tile->n );
}

static uint32_t
ti_alloc_footprint( starpu_data_handle_t handle )
{
    return starpu_hash_crc32c_be( ti_handle_get_allocsize(handle), 0 );
}

static int
ti_compare( void *data_interface_a, void *data_interface_b )
{
    starpu_tile_interface_t *_tile_a = (starpu_tile_interface_t *) data_interface_a;
    starpu_tile_interface_t *_tile_b = (starpu_tile_interface_t *) data_interface_b;

    /* Two matrices are considered compatible if they have the same size */
    return ( _tile_a->tile.m  == _tile_b->tile.m  )
        && ( _tile_a->tile.n  == _tile_b->tile.n  );
}

static int
ti_alloc_compare(void *data_interface_a, void *data_interface_b)
{
    starpu_tile_interface_t *_tile_a = (starpu_tile_interface_t *) data_interface_a;
    starpu_tile_interface_t *_tile_b = (starpu_tile_interface_t *) data_interface_b;

    /* Two matrices are considered compatible if they have the same allocated size */
    return ( _tile_a->allocsize   == _tile_b->allocsize   );
}

static void
ti_display( starpu_data_handle_t handle, FILE *f )
{
    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node(handle, STARPU_MAIN_RAM);

    fprintf( f, "%u\t%u\t",
             tile_interface->tile.m,
             tile_interface->tile.n );
}

static int
ti_pack_data_fullrank( starpu_tile_interface_t *tile_interface,
                        void *ptr )
{
    char *matrix = (void *)tile_interface->tile.c;

    if ( tile_interface->tile.m == tile_interface->tile.ld ) {
        memcpy( ptr, matrix, tile_interface->allocsize );
    }
    else {
        int   n;
        char *tmpptr = ptr;

        for(n=0; n<tile_interface->tile.n; n++)
        {
            size_t elemsize = sizeof(double);
            size_t size = tile_interface->tile.m * elemsize;
            memcpy( tmpptr, matrix, size );
            tmpptr += size;
            matrix += tile_interface->tile.ld * elemsize;
        }
    }
    return 0;
}

static int
ti_pack_data( starpu_data_handle_t handle, unsigned node, void **ptr, starpu_ssize_t *count )
{
    STARPU_ASSERT(starpu_data_test_if_allocated_on_node(handle, node));

    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node(handle, node);
    size_t size;

    size   = (starpu_ssize_t)(tile_interface->allocsize);
    *count = size + sizeof(size_t) + sizeof(Block);

    if ( ptr != NULL )
    {
        char *tmp;
        *ptr = (void *)starpu_malloc_on_node_flags( node, *count, 0 );
        tmp = (char*)(*ptr);

        /* Start by the size to allocate on reception */
        memcpy( tmp, &size, sizeof(size_t) );
        tmp += sizeof(size_t);

        /* Copy the tile metadata */
        memcpy( tmp, &(tile_interface->tile), sizeof(Block) );
        tmp += sizeof(Block);

        /* Pack the real data */
        ti_pack_data_fullrank( tile_interface, tmp );
    }

    return 0;
}


static int
ti_unpack_data_fullrank( starpu_tile_interface_t *tile_interface,
                          void *ptr )
{
    char *matrix = (void *)tile_interface->tile.c;
    assert( matrix != NULL );

    if ( tile_interface->tile.m == tile_interface->tile.ld ) {
        memcpy( matrix, ptr, tile_interface->allocsize );
    }
    else {
        int   n;
        char *tmpptr = ptr;

        for(n=0 ; n<tile_interface->tile.n; n++)
        {
            size_t elemsize = sizeof(double);
            size_t size = tile_interface->tile.m * elemsize;
            memcpy( matrix, tmpptr, size );
            tmpptr += size;
            matrix += tile_interface->tile.ld * elemsize;
        }
    }
    return 0;
}


static int
ti_peek_data( starpu_data_handle_t handle, unsigned node, void *ptr, size_t count )
{
    STARPU_ASSERT(starpu_data_test_if_allocated_on_node(handle, node));

    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node(handle, node);

    char *tmp = ptr;

    /*
 *      * We may end up here if an early reception occured before the handle of the
 *           * received data has been registered. Thus, datatype was not existant and we
 *                * need to unpack the data ourselves
 *                     */
    STARPU_ASSERT( count == tile_interface->allocsize );
    /* Unpack the real data */
    ti_unpack_data_fullrank( tile_interface, tmp );

    return 0;
}

static int
ti_unpack_data( starpu_data_handle_t handle, unsigned node, void *ptr, size_t count )
{
    ti_peek_data( handle, node, ptr, count );

    /* Free the received information */
    starpu_free_on_node_flags( node, (uintptr_t)ptr, count, 0 );

    return 0;
}

static starpu_ssize_t
ti_describe( void *data_interface, char *buf, size_t size )
{
    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *) data_interface;
    return snprintf( buf, size, "M%ux%ux8",
                     (unsigned) tile_interface->tile.m,
                     (unsigned) tile_interface->tile.n);
}

static int ti_copy_any_to_any( void *src_interface, unsigned src_node,
                                void *dst_interface, unsigned dst_node, void *async_data )
{
    starpu_tile_interface_t *_tile_src = (starpu_tile_interface_t *) src_interface;
    starpu_tile_interface_t *_tile_dst = (starpu_tile_interface_t *) dst_interface;
    size_t elemsize = sizeof(double);
    size_t m = _tile_src->tile.m;
    size_t n = _tile_src->tile.n;
    size_t ld_src = _tile_src->tile.ld;
    size_t ld_dst = _tile_dst->tile.ld;
    int ret = 0;

    void *src_mat =  &(_tile_src->tile).c ;
    void *dst_mat =  &(_tile_dst->tile).c ;

    ld_src *= elemsize;
    ld_dst *= elemsize;
    if (starpu_interface_copy2d( (uintptr_t) src_mat, 0, src_node,
                                 (uintptr_t) dst_mat, 0, dst_node,
                                 m * elemsize, n, ld_src, ld_dst, async_data ) ) {
        ret = -EAGAIN;
    } 

    starpu_interface_data_copy( src_node, dst_node, (size_t) n*m*elemsize );

    return ret;
}

int
ti_allocate_datatype_node( starpu_data_handle_t handle,
                            unsigned             node,
                            MPI_Datatype        *datatype )
{
    int ret;

    starpu_tile_interface_t *_tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node( handle, node );

    size_t m  = _tile_interface->tile.m;
    size_t n  = _tile_interface->tile.n;
    size_t ld = _tile_interface->tile.ld;
    size_t elemsize = sizeof(double);

    ret = MPI_Type_vector( n, m * elemsize, ld * elemsize, MPI_BYTE, datatype );
    STARPU_ASSERT_MSG(ret == MPI_SUCCESS, "MPI_Type_vector failed");

    ret = MPI_Type_commit( datatype );
    STARPU_ASSERT_MSG(ret == MPI_SUCCESS, "MPI_Type_commit failed");

    return 0;
}

int
ti_allocate_datatype( starpu_data_handle_t handle,
                       MPI_Datatype        *datatype )
{
    return ti_allocate_datatype_node( handle, STARPU_MAIN_RAM, datatype );
}

void
ti_free_datatype( MPI_Datatype *datatype )
{
    MPI_Type_free( datatype );
}

int
ti_handle_get_m( starpu_data_handle_t handle )
{
    Block *tile = ti_handle_get( handle );
    return tile->m;
}

int
ti_handle_get_n( starpu_data_handle_t handle )
{
    Block *tile = ti_handle_get( handle );
    return tile->n;
}

int
ti_handle_get_ld( starpu_data_handle_t handle )
{
    Block *tile = ti_handle_get( handle );
    return tile->ld;
}

size_t
ti_handle_get_allocsize( starpu_data_handle_t handle )
{
    starpu_tile_interface_t *tile_interface = (starpu_tile_interface_t *)
        starpu_data_get_interface_on_node( handle, STARPU_MAIN_RAM );

    return tile_interface->allocsize;
}

static const struct starpu_data_copy_methods ti_copy_methods =
{
    .any_to_any = ti_copy_any_to_any,
};


struct starpu_data_interface_ops starpu_interface_tile_ops =
{
    .init                  = ti_init,
    .register_data_handle  = ti_register_data_handle,
    .allocate_data_on_node = ti_allocate_data_on_node,
    .free_data_on_node     = ti_free_data_on_node,
    .to_pointer            = ti_to_pointer,
    .pointer_is_inside     = ti_pointer_is_inside,
    .get_size              = ti_get_size,
    .get_alloc_size        = ti_get_alloc_size,
    .footprint             = ti_footprint,
    .alloc_footprint       = ti_alloc_footprint,
    .compare               = ti_compare,
    .alloc_compare         = ti_alloc_compare,
    .display               = ti_display,
#if defined (HAVE_STARPU_DATA_PEEK)
    .peek_data             = ti_peek_data,
#endif
    .pack_data             = ti_pack_data,
    .unpack_data           = ti_unpack_data,
    .describe              = ti_describe,
    .copy_methods          =&ti_copy_methods,
    .interfaceid           = STARPU_UNKNOWN_INTERFACE_ID,
    .interface_size        = sizeof(starpu_tile_interface_t),
    .name                  = "STARPU_TILE_INTERFACE"
};


void
starpu_tile_register( starpu_data_handle_t *handleptr,
                      int                   home_node,
                      Block          *tile)
{
    size_t elemsize = sizeof(double);
    starpu_tile_interface_t _tile_interface =
     {
         .id         = starpu_interface_tile_ops.interfaceid,
         .dev_handle = (intptr_t)(tile->c),
         .allocsize  = -1,
         .tilesize   = tile->m * tile->n * elemsize,
     };
    memcpy( &(_tile_interface.tile), tile, sizeof( Block ) );
    _tile_interface.allocsize = tile->m * tile->n * elemsize;
    starpu_data_register( handleptr, home_node, &_tile_interface, &starpu_interface_tile_ops );
}


void
starpu_tile_interface_register()
{
    if ( starpu_interface_tile_ops.interfaceid == STARPU_UNKNOWN_INTERFACE_ID )
    {
        starpu_interface_tile_ops.interfaceid = starpu_data_interface_get_next_id();
#if defined (HAVE_STARPU_MPI_INTERFACE_DATATYPE_NODE_REGISTER)
        starpu_mpi_interface_datatype_node_register( starpu_interface_tile_ops.interfaceid,
                                                    ti_allocate_datatype_node,
                                                    ti_free_datatype );
#else
        starpu_mpi_interface_datatype_register( starpu_interface_tile_ops.interfaceid,
                                                    ti_allocate_datatype,
                                                    ti_free_datatype );
#endif
    }
}

void
starpu_tile_interface_unregister()
{
    if ( starpu_interface_tile_ops.interfaceid != STARPU_UNKNOWN_INTERFACE_ID )
    {
        starpu_mpi_interface_datatype_unregister( starpu_interface_tile_ops.interfaceid );
    }
}

