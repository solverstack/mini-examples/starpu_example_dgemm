#ifndef TILE_INTERFACE_STANDALONE_H
#define TILE_INTERFACE_STANDALONE_H

#include <starpu.h>
#include "dsmat.h"

extern struct starpu_data_interface_ops starpu_interface_tile_ops;

typedef struct starpu_tile_interface_s
{
    enum starpu_data_interface_id id; /**< Identifier of the interface           */
    uintptr_t      dev_handle;        /**< device handle of the matrix           */
    size_t         allocsize;         /**< size actually currently allocated     */
    size_t         tilesize;          /**< size of the elements of the matrix    */
    Block    tile;              /**< Internal tile structure used to store
                                           information on non memory home_node   */
} starpu_tile_interface_t;

void starpu_tile_register( starpu_data_handle_t *handleptr,
                                int            home_node,
                                Block          *tile);

int    ti_handle_get_m        ( starpu_data_handle_t handle );
int    ti_handle_get_n        ( starpu_data_handle_t handle );
size_t ti_handle_get_allocsize( starpu_data_handle_t handle );
static inline Block* ti_interface_get(starpu_tile_interface_t *interface) {
	return &(interface->tile);
}

int ti_allocate_datatype( starpu_data_handle_t handle,
                       MPI_Datatype        *datatype );
void ti_free_datatype( MPI_Datatype        *datatype );

void starpu_tile_interface_register();
void starpu_tile_interface_unregister();

#endif
