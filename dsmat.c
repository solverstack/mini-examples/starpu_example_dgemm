#include <starpu.h>
#include <starpu_mpi.h>

#include "dsmat.h"
#include "optional_tile_interface.h"

int comm_rank; /* mpi rank of the process */

Matrix* alloc_matrix(int mb, int nb, int b, int p, int q, starpu_mpi_tag_t* tag)
{
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	Matrix* X;
	X = calloc(1, sizeof(Matrix));
      	X->blocks = calloc( mb*nb,sizeof(Block));
	int i,j;
//	printf("[%d] allocating %d x %d B %d on %dx%d\n", comm_rank, mb, nb, b, p, q);
	for (i = 0; i<mb; i++)
	{
		for (j= 0; j<nb; j++)
		{
			X->blocks[i*nb+j].owner = (i%p)*q + (j%q);
			X->blocks[i*nb+j].m  = b;
			X->blocks[i*nb+j].n  = b;
			X->blocks[i*nb+j].ld = b;
			X->blocks[i*nb+j].tag= *tag;
			X->blocks[i*nb+j].registered = 0;
			//X->blocks[i*nb+j].hdl = NULL;
			if (X->blocks[i*nb+j].owner == comm_rank)
  				X->blocks[i*nb+j].c = malloc(b*b*sizeof(double));
			else
				X->blocks[i*nb+j].c = NULL;
			*tag = *tag + 1;
		}
	}
	X->mb = mb;
	X->nb = nb;
       	X->b  = b;
	return X;
}

void free_matrix(Matrix* X)
{
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	int i,j;
	Block* Xij;
	int mb = X->mb, nb = X->nb;
	for (i = 0; i<mb; i++)
	{
		for (j= 0; j<nb; j++)
		{
			Xij = & X->blocks[i*nb + j];
			if (Xij->owner == comm_rank) {
				free(Xij->c);
			}
		}
	}
	free(X->blocks);
	free(X);
}

void register_matrix(Matrix* X, int mb, int nb, char* name, int datatype, int prune_handles, int p, int q, int row, int col, int check, int delay)
{
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	int proc_row, proc_col;
	int b_row, b_col;
	Block* Xij;
	// comm_rank = proc_row * q + proc_col
    	proc_col = comm_rank % q;
	proc_row = (comm_rank - proc_col)/q; 
//	printf("[%d = %d x %d | %d x %d] delayed ? %d / check ? %d / dt ? %d / pr_hd ? %d\n", 
//		comm_rank, proc_row, proc_col, row, col, delay, check, datatype, prune_handles);
	for (b_row = 0; b_row < mb; b_row++)
	{
		for (b_col = 0; b_col < nb; b_col++)
		{
			Xij = & X->blocks[b_row*nb+b_col];
	    		if (Xij->owner == comm_rank)
			{
//				printf("[%d] %s_%d,%d=%dx%d (%p) | tag:%ld | my registration \n",comm_rank,name,b_row,b_col,b_row%p,b_col%q,Xij->hdl,Xij->tag);
				if (datatype) {
			  		starpu_tile_register( &Xij->hdl, STARPU_MAIN_RAM, Xij ); 
			  	} else {
					starpu_matrix_data_register( &Xij->hdl, STARPU_MAIN_RAM,
							    (uintptr_t) Xij->c, Xij->m, Xij->n, Xij->ld,
							    sizeof(double));
			     	}
				starpu_mpi_data_register(Xij->hdl, Xij->tag, Xij->owner);
				Xij->registered = 1;
			} else if (!delay && (!prune_handles || (row && proc_row == b_row % p) ||
						     (col && proc_col == b_col % q) ||
						     (check && Xij->owner == 0) || 
						     (check && comm_rank == 0)) ) {
			//	printf("[%d] %s_%d,%d=%dx%d (%p) | tag:%ld | registered for %d\n",comm_rank,name,b_row,b_col,b_row%p,b_col%q,Xij->hdl,Xij->tag,Xij->owner);
				block_starpu_register(Xij, datatype);
			} else {
			//	printf("[%d] %s_%d,%d=%dx%d (%p) | tag:%ld | owned by %d - I don't register\n",comm_rank,name,b_row,b_col,b_row%p,b_col%q,Xij->hdl,Xij->tag,Xij->owner);
			}
		}
	}
}

void unregister_matrix(Matrix* X, int mb, int nb)
{
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
//	printf("[%d]unregistering %dx%d matrix\n", comm_rank, mb, nb);
	int b_row,b_col;
	Block* Xij;
	for (b_row = 0; b_row < mb; b_row++)
	{
		for (b_col = 0; b_col < nb; b_col++)
		{
			// assuming we flush, we do not need to unregister everywhere
			Xij = & X->blocks[b_row*nb + b_col];	
			if (Xij->owner == comm_rank) {
//				printf("[%d] unregistering X_%d,%d\n", comm_rank, b_row, b_col);
 				starpu_data_unregister(Xij->hdl);
			}
/*			free(X->blocks[b_row*nb+b_col].hdl);
			X->blocks[b_row*nb+b_col].hdl = NULL;
*/			Xij->registered = 0;
		}
	}
}


void print_block(Block* X, int i, int j, char* name) {
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	int b_row,b_col;
	for (b_row = 0; b_row < X->m; b_row++) {
		for (b_col = 0; b_col < X->n; b_col++) {
			printf("[%d] %s_%d,%d (%d,%d) = %f\n",comm_rank, name, i,j,b_row,b_col, X->c[b_row*X->n + b_col]);	
		}
	}
}
void print_matrix(Matrix* X, char* name) {
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	int i,j;
 	for (i = 0 ; i < X->mb ; i++) {
 		for (j = 0 ; j < X->nb ; j++) {
			if (comm_rank == X->blocks[i*X->nb+j].owner)
				print_block(&X->blocks[i*X->nb + j], i, j, name);	
		}	
	}	
}

void block_starpu_register(Block* Xij, int datatype) {
	if (!Xij->registered) {
		starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
//		Xij->hdl = malloc(sizeof(starpu_data_handle_t));
		if (datatype) {
			starpu_tile_register( &Xij->hdl, -1, Xij );
		} else {
		  	starpu_matrix_data_register( &Xij->hdl, -1, 
				    (uintptr_t) NULL, Xij->m, Xij->n, Xij->ld,
				    sizeof(double));
		}
		starpu_mpi_data_register(Xij->hdl, Xij->tag, Xij->owner);
//		printf("[%d] X_block | mpi_data_register %p\n",comm_rank,*Xij->hdl);
		Xij->registered = 1;
	} else {
//		printf("[%d] X_block | already registered\n");
	}
}
