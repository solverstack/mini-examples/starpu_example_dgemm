! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2016-2021  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
program fstarpu_example_dgemm
  use iso_c_binding       ! C interfacing module
  use fstarpu_mod         ! StarPU interfacing module
  use fstarpu_mpi_mod     ! StarPU-MPI interfacing module
  use fstarpu_codelets

  type block_type
     real(kind=c_double), allocatable :: c(:,:)
     type(c_ptr)                      :: h = c_null_ptr
     integer                          :: owner
     integer(c_int64_t)               :: tag 
  end type block_type

  type dsmat_type
     integer                       :: m, n, b
     type(block_type), allocatable :: blocks(:,:)
  end type dsmat_type


  logical, parameter              :: verbose = .false.
  logical                         :: trace = .false.
  logical                         :: lflush = .false.
  logical                         :: prune = .false.
  logical                         :: super_prune = .false.
  logical                         :: prune_handles = .false.
  logical                         :: delay = .false.
  logical                         :: provide_context = .false.
  logical                         :: warmup = .true.
  logical                         :: codelet_args = .false.
  logical                         :: outer = .false.
  integer(c_int)                  :: comm_size, comm_rank
  integer(c_int), target          :: comm_world

  integer                         :: bs
  integer(c_int)                  :: m, mb
  integer(c_int)                  :: n, nb
  integer(c_int)                  :: k, kb
  character(len=20)               :: str

  type(dsmat_type),target         :: A, B, C
  logical                         :: A_local, B_local, C_local
  real(kind=c_double), target     :: alpha, beta, zbeta
  type(c_ptr)                     :: cl_mm, cl_mm_args, cl_fill
  integer(c_int)                  :: ncpu
  integer(c_int)                  :: ret
  integer                         :: i, j, l, p , q, trial, t
  integer                         :: te, ts, tr
  real                            :: tf, gflops

  integer(c_int), dimension(:), allocatable :: procs
  integer(c_int)                            :: ctx
  integer(c_int),target                     :: arg_ctx
  character(kind=c_char,len=4), target      :: ctx_policy = C_CHAR_"lws"//C_NULL_CHAR

  write(*,*) "initializing starpu ..."
  ret = fstarpu_init(C_NULL_PTR)
  if (ret == -19) then
     stop 77
  else if (ret /= 0) then
     stop 1
  end if

  write(*,*) "initializing starpu+MPI ..."
  ret = fstarpu_mpi_init(1)
  if (ret /= 0) then
     write(*,'("fstarpu_mpi_init status:",i4)') ret
     stop 2
  end if
  write(*,*) "initialized starpu+MPI!"

  ! stop there if no CPU worker available
  ncpu = fstarpu_cpu_worker_get_count()
  if (ncpu == 0) then
     write(*,'("no cpu workers available")')
     call fstarpu_shutdown()
     stop 79
  end if

  comm_world = fstarpu_mpi_world_comm()
  comm_size  = fstarpu_mpi_world_size()
  comm_rank  = fstarpu_mpi_world_rank()

  call get_command_argument(1, value=str, length=i)
  read(str(1:i),*) m
  call get_command_argument(2, value=str, length=i)
  read(str(1:i),*) n
  call get_command_argument(3, value=str, length=i)
  read(str(1:i),*) k 
  call get_command_argument(4, value=str, length=i)
  read(str(1:i),*) bs
  call get_command_argument(5, value=str, length=i)
  read(str(1:i),*) p
  call get_command_argument(6, value=str, length=i)
  read(str(1:i),*) q
  call get_command_argument(7, value=str, length=i)
  read(str(1:i),*) t
  do j = 8, command_argument_count()
    call get_command_argument(j, value=str, length=i)
    select case(str)
      case('-t')
        trace = .true.
      case('-f')
        lflush = .true.
      case('-p')
        prune = .true.
        super_prune = .false.
      case('-s')
        prune = .false.
        super_prune = .true.
      case('-h')
        prune_handles = .true.
      case('-d')
        delay = .true.
      case('-c')
        provide_context = .true.
      case('-now')
        warmup = .false.
      case('-a')
        codelet_args = .true.
      case('-o')
        outer = .true.
      ! keep -e as an empty argument for debug purpose
    end select
  end do
  
  if (mod(m,bs).ne.0) stop 65
  if (mod(n,bs).ne.0) stop 64
  if (mod(k,bs).ne.0) stop 63
  if (p*q.ne.comm_size) stop 74
  mb = m/bs
  nb = n/bs
  kb = k/bs
  if (comm_rank.eq.0) then
     write(*,'("========================================")')
     write(*,'("mxnxk    = ",i5,"x",i5,"x",i5)') m, n, k
     write(*,'("mbxnbxkb = ",i5,"x",i5,"x",i5)') mb, nb, kb
     write(*,'("B        = ",i5)') bs
     write(*,'("PxQ      = ",i3,"x",i3)') p,q
     if (trace)           write(*,*) "(T)racing enabled"
     if (lflush)          write(*,*) "(F)lushing enabled"
     if (super_prune)     write(*,*) "(S)uper-pruning enabled"
     if (prune)           write(*,*) "(P)runing enabled"
     if (prune_handles)   write(*,*) "(H)andles pruning enabled"
     if (delay)           write(*,*) "(D)elayed handle registration enabled"
     if (provide_context) write(*,*) "(C)ontext provided at submission"
     if (.not.warmup)     write(*,*) "(W)armup disabled"
     if (codelet_args)    write(*,*) "(A)rguments NOT by values"
     if (outer)           write(*,*) "(O)uter product submission"
     write(*,'("========================================")')
  end if
  ret = fstarpu_mpi_barrier(comm_world)

  ! intialize codelets
  call initialize_codelets()
  alpha = 0.42
  beta = 3.14

  if (provide_context) then
    allocate(procs(ncpu))
    err = fstarpu_worker_get_ids_by_type(FSTARPU_CPU_WORKER, procs, ncpu)
    ctx = fstarpu_sched_ctx_create(procs, ncpu, C_CHAR_"stdalone"//C_NULL_CHAR,&
        (/ FSTARPU_SCHED_CTX_POLICY_NAME, c_loc(ctx_policy), c_null_ptr /) )
  end if

  if (warmup) t = t + 1
  do trial=1,t
     ! allocate matrices
     call initialize_matrix(A,mb,kb,"A",.true. ,.false.)
     call initialize_matrix(B,kb,nb,"B",.false.,.true.)
     call initialize_matrix(C,mb,nb,"C",.true. ,.true.)
     ret = fstarpu_mpi_barrier(comm_world)

     call fill_matrix(A, mb,kb,"A")
     call fill_matrix(B, kb,nb,"B")
     call fill_matrix(C, mb,nb,"C")
     ret = fstarpu_mpi_wait_for_all(comm_world)
     ret = fstarpu_mpi_barrier(comm_world)

     call system_clock(ts)
     ! submit matrix multiplication
     if (outer) then 
       do l=1,kb
         do i=1,mb
           do j=1,nb
             call submit_gemm_task()
           end do
         end do
         if (lflush) then
           do i=1,mb
             if (c_associated(A%blocks(i,l)%h)) &
               call fstarpu_mpi_cache_flush(comm_world,A%blocks(i,l)%h)
           end do
           do j=1,nb
             if (c_associated(B%blocks(l,j)%h)) &
               call fstarpu_mpi_cache_flush(comm_world,B%blocks(l,j)%h)
           end do
         end if
       end do
     else
       do i=1,mb
         do j=1,nb
           do l=1,kb
             call submit_gemm_task()
           end do
         end do
         if (lflush) then
           do l=1,kb
             if (c_associated(A%blocks(i,l)%h)) &
               call fstarpu_mpi_cache_flush(comm_world,A%blocks(i,l)%h)
           end do
         end if
       end do
     end if
     
     ret = fstarpu_mpi_wait_for_all(comm_world)
     ret = fstarpu_mpi_barrier(comm_world)
     call system_clock(te,tr)
     tf = max(real(te-ts)/real(tr),1e-20)
     gflops = 2.0*m*n*k/(tf*10**9)
     if (comm_rank.eq.0.and.(.not.warmup.or.trial.gt.1)) &
        write(*,'("RANK ",i3," -> took ",e15.8," s | ", e15.8," Gflop/s")') &
             comm_rank, tf, gflops
     
      
     call fstarpu_mpi_cache_flush_all_data(comm_world)
     ! unregister matrices
     call unregister_matrix(A,mb,kb)
     call unregister_matrix(B,kb,nb)
     call unregister_matrix(C,mb,nb)
  end do
  
  if (provide_context) then
     call fstarpu_sched_ctx_delete(ctx)
     deallocate(procs)
  endif
  
  call fstarpu_codelet_free(cl_mm)
  call fstarpu_codelet_free(cl_mm_args)
  call fstarpu_codelet_free(cl_fill)
  call fstarpu_shutdown()
  
  ret = fstarpu_mpi_shutdown()
  if (ret /= 0) then
     write(*,'("fstarpu_mpi_shutdown status:",i4)') ret
     stop 1
  end if
  
contains

  subroutine submit_gemm_task()
    implicit none
           A_local = A%blocks(i,l)%owner == comm_rank
           B_local = B%blocks(l,j)%owner == comm_rank
           C_local = C%blocks(i,j)%owner == comm_rank
           if ( &
        ( .not.super_prune.or.(C_local.or.(A_local.and.j<=q).or.(B_local.and.i<=p)) ).and.&
        ( .not.prune.or.(A_local.or.B_local.or.C_local))                            ) then
 !if (c_local) then;  write(*,*) comm_rank,"] executing",i,j,l,"with A",c_associated(A%blocks(i,l)%h),&
 !                                    "with B",c_associated(B%blocks(l,j)%h),&
 !                                    "with C",c_associated(C%blocks(i,j)%h)
 !else;       write(*,*) comm_rank,"] inserting",i,j,l,"with A",c_associated(A%blocks(i,l)%h),&
 !                                    "with B",c_associated(B%blocks(l,j)%h),&
 !                                    "with C",c_associated(C%blocks(i,j)%h)
 !endif
             ! if (comm_rank.eq.0) write(*,*) "GEMM", b_col,b_row,b_aisle
             if (l.eq.1) then; zbeta = beta; else; zbeta = 1.0d0; end if
             if (delay) then
                call block_register(A,i,l)
                call block_register(B,l,j)
                call block_register(C,i,j)
             end if
             if (provide_context) then
                 arg_ctx = ctx
                 call fstarpu_mpi_task_insert((/ c_loc(comm_world), cl_mm, &
                    FSTARPU_VALUE, c_loc(alpha), FSTARPU_SZ_REAL8,       &
                    FSTARPU_VALUE, c_loc(zbeta), FSTARPU_SZ_REAL8,       &
                    FSTARPU_R,  A%blocks(i,l)%h,                         &
                    FSTARPU_R,  B%blocks(l,j)%h,                         &
                    FSTARPU_RW, C%blocks(i,j)%h,                         &
                    FSTARPU_SCHED_CTX, c_loc(arg_ctx),            &
                    c_null_ptr /))
             else if (codelet_args) then 
               call insert_gemm_task()
             else 
               call fstarpu_mpi_task_insert((/ c_loc(comm_world), cl_mm, &
                  FSTARPU_VALUE, c_loc(alpha), FSTARPU_SZ_REAL8,       &
                  FSTARPU_VALUE, c_loc(zbeta), FSTARPU_SZ_REAL8,       &
                  FSTARPU_R,  A%blocks(i,l)%h,                         &
                  FSTARPU_R,  B%blocks(l,j)%h,                         &
                  FSTARPU_RW, C%blocks(i,j)%h,                         &
                  c_null_ptr /))
             end if
           else
              !could write something 
           end if
  end subroutine submit_gemm_task
  
  subroutine initialize_codelets()
    implicit none
    cl_mm = fstarpu_codelet_allocate()
    call fstarpu_codelet_set_name(cl_mm, c_char_"nf_gemm_cl"//c_null_char)
    call fstarpu_codelet_add_cpu_func(cl_mm, C_FUNLOC(cl_cpu_gemm))
    call fstarpu_codelet_add_buffer(cl_mm, FSTARPU_R)
    call fstarpu_codelet_add_buffer(cl_mm, FSTARPU_R)
    call fstarpu_codelet_add_buffer(cl_mm, FSTARPU_RW)
    cl_mm_args = fstarpu_codelet_allocate()
    call fstarpu_codelet_set_name(cl_mm_args, c_char_"nf_gemm_cl_args"//c_null_char)
    call fstarpu_codelet_add_cpu_func(cl_mm_args, C_FUNLOC(cl_cpu_gemm_args))
    call fstarpu_codelet_add_buffer(cl_mm_args, FSTARPU_R)
    call fstarpu_codelet_add_buffer(cl_mm_args, FSTARPU_R)
    call fstarpu_codelet_add_buffer(cl_mm_args, FSTARPU_RW)
    cl_fill = fstarpu_codelet_allocate()
    call fstarpu_codelet_set_name(cl_fill, c_char_"nf_fill_cl"//c_null_char)
    call fstarpu_codelet_add_cpu_func(cl_fill, C_FUNLOC(cl_cpu_fill))
    call fstarpu_codelet_add_buffer(cl_fill, FSTARPU_W)
  end subroutine initialize_codelets

  subroutine initialize_matrix(X,mb,nb,cname,prune_row,prune_col)
    implicit none
    type(dsmat_type), target        :: x
    integer                         :: mb, nb  
    character                       :: cname

    integer                         :: i, j
    type(block_type), pointer       :: xij
    integer(c_int64_t), save        :: tag = 1
    logical, optional               :: prune_row, prune_col
    logical                         :: lrow, lcol
    integer                         :: comm_row, comm_col
    if (present(prune_row)) then; lrow = prune_row; else; lrow = .false.; endif;
    if (present(prune_col)) then; lcol = prune_col; else; lcol = .false.; endif;
    x%m = mb*bs
    x%n = nb*bs
    x%b = bs
    allocate(x%blocks(mb,nb))
    ! comm_rank = q*comm_row + comm_col
    comm_col = mod(comm_rank, q)
    comm_row = (comm_rank - comm_col)/q
    do i=1,mb
       do j=1,nb
          xij => x%blocks(i,j) 
          xij%owner = mod(i-1,p)*q + mod(j-1,q)
          xij%tag = tag
          if (comm_rank.eq.xij%owner) then
             ! write(*,*) comm_rank,"] I own ",cname,"_",i,j,"so I register it with tag",tag  
             allocate(xij%c(bs,bs))
             call fstarpu_matrix_data_register( xij%h, 0, c_loc( xij%c(1,1) ), &
                  bs, bs, bs, c_sizeof(xij%c(1,1)) )
             call fstarpu_mpi_data_register(xij%h, tag, xij%owner)
          else if (.not.delay.and.(.not.prune_handles.or.(lrow.and.comm_row.eq.mod(i-1,p)).or.&
                                         (lcol.and.comm_col.eq.mod(j-1,q))) ) then 
          !   write(*,*) comm_rank,"] ",xij%owner," owns ",cname,"_",i,j,"so it registers it with tag",tag  
             call fstarpu_matrix_data_register( xij%h, -1, c_null_ptr, &
                  bs, bs, bs, c_sizeof(alpha) )
             call fstarpu_mpi_data_register(xij%h, tag, xij%owner)
          end if
          tag = tag + 1
       end do
    end do
  end subroutine initialize_matrix

  subroutine block_register(X,i,j)
    type(dsmat_type), target        :: x
    integer                         :: mb, nb  
    if (c_associated(x%blocks(i,j)%h)) return
    call fstarpu_matrix_data_register( x%blocks(i,j)%h, -1, c_null_ptr, &
         x%b, x%b, x%b, c_sizeof(alpha) )
    call fstarpu_mpi_data_register(x%blocks(i,j)%h, x%blocks(i,j)%tag, x%blocks(i,j)%owner)
  end subroutine block_register

  subroutine fill_matrix(x,mb,nb,cname)
    implicit none
    type(dsmat_type), target        :: x
    integer                         :: mb, nb  
    character                       :: cname

    integer                         :: i, j
    type(block_type), pointer       :: xij

    do i=1,mb
       do j=1,nb
          xij => x%blocks(i,j) 
          if (comm_rank.eq.xij%owner) then
             ! write(*,*) comm_rank,"] I own ",cname,"_",i,j,"so I fill it"  
             call fstarpu_mpi_task_insert((/ c_loc(comm_world), cl_fill, &
                  FSTARPU_W, xij%h, &
                  c_null_ptr /))
          else
             !write(*,*) comm_rank,"] ",xij%owner,"owns ",cname,"_",i,j,"so it fills it"  
          end if
       end do
    end do
  end subroutine fill_matrix

  subroutine insert_gemm_task()
    implicit none
    type(cl_gemm_args), pointer     :: cl_args
    integer(c_int), pointer         :: args_sz

    if (C_local) then
      allocate(cl_args,args_sz)
      cl_args%alpha = alpha
      cl_args%beta = zbeta
      args_sz = storage_size(cl_args)/8
    end if
    call fstarpu_mpi_task_insert((/ c_loc(comm_world), cl_mm_args, &
       FSTARPU_CL_ARGS, c_loc(cl_args), c_loc(args_sz),     &
       FSTARPU_R,  A%blocks(i,l)%h,                         &
       FSTARPU_R,  B%blocks(l,j)%h,                         &
       FSTARPU_RW, C%blocks(i,j)%h,                         &
       c_null_ptr /))
    ! We do not deallocate ... which is weird ... but the task does
    ! it for us upon destruction 

  end subroutine insert_gemm_task
  
  subroutine unregister_matrix(x,mb,nb)
    implicit none
    integer                         :: mb, nb  
    type(block_type), pointer       :: xij
    type(dsmat_type), target        :: x

    integer         :: i, j

    do i=1,mb
       do j=1,nb
          xij => x%blocks(i,j) 
          if (comm_rank.eq.xij%owner) then
             call fstarpu_data_unregister(xij%h)
             deallocate(xij%c)
          end if
       end do
    end do
    deallocate(x%blocks)
  end subroutine unregister_matrix

end program
