#ifndef TILE_INTERFACE_STANDALONE_H
#define TILE_INTERFACE_STANDALONE_H

#include <starpu.h>
#include "dsmat.h"

extern struct starpu_data_interface_ops starpu_interface_matrix_ops;

typedef struct starpu_matrix_interface_s
{
    enum starpu_data_interface_id id; /**< Identifier of the interface           */
    uintptr_t      dev_handle;        /**< device handle of the matrix           */
    size_t         allocsize;         /**< size actually currently allocated     */
    size_t         matrixsize;          /**< size of the elements of the matrix    */

    Block* matrix;
} starpu_matrix_interface_t;

void starpu_my_matrix_register( starpu_data_handle_t *handleptr,
                                int            home_node,
                                Block          *matrix);

int    mi_handle_get_m        ( starpu_data_handle_t handle );
int    mi_handle_get_n        ( starpu_data_handle_t handle );
size_t mi_handle_get_allocsize( starpu_data_handle_t handle );
static inline Block* mi_interface_get(starpu_matrix_interface_t *interface) {
	return &(interface->matrix);
}

int mi_allocate_datatype( starpu_data_handle_t handle,
                       MPI_Datatype        *datatype );
void mi_free_datatype( MPI_Datatype        *datatype );

void starpu_matrix_interface_register();
void starpu_matrix_interface_unregister();

#endif
