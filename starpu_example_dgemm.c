/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2016-2021  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

/*
 * This example illustrates the computation of general matrices with originally
 * distributed A, B and C matrices to a set of computing nodes.
 */

#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <argp.h>

#include <starpu.h>
#include <starpu_mpi.h>
#include <cblas.h>
#include <lapacke.h>

#include "dsmat.h"
#include "optional_tile_interface.h"

/* STARPU_EXAMPLE_DGEMM default values */
// line 142 and below

/* From StarPU starpu/tests/helper.c */
#define STARPU_TEST_SKIPPED 77

//const char *argp_program_version = "standalone 0.2";
//const char *argp_program_bug_address
static char doc[] = "Standalone DGEMM using StarPU-MPI";
static char args_doc[] = "-m [m] -n [n] -k [k] -b [b] -p [p] -q [q] --niter [l] [--check] [--trace] [--datatype] [--mpi-thread [t]] [--no-flush]  [--prune] [--prune-handles] [--super-prune]";
static struct argp_option options[] = {
       {"m",   	        'm', "int", 0, "Number of rows in A and C (deprecated)" },
       {"n",            'n', "int", 0, "Dimension of A B and C" },
       {"k",            'k', "int", 0, "Shared dimension of A and B (deprecated)" },
       {"blocking",     'b', "int", 0, "Size of the square block of A, B and C (must divide m,n and k" },
       {"p",            'p', "int", 0, "Length of the logical grid"},
       {"q",            'q', "int", 0, "Width of the logical grid"},
       {"check",        'c',     0, 0, "If the program checks gemm results (NOT INTERPRETED)"},
       {"verbose",      'v',     0, 0, "Extend program vebosity"},
       {"trace",        'f',     0, 0, "Whether or not to use FxT tracing"},
       {"niter",        'l', "int", 0, "Number of iterations (loops)"},
       {"mpi-thread",   't', "int", 0, "MPI thread level support : -1 StarPU, 0 SINGLE, 1 FUNNELED, 2 SERIALIZED, 3 MULTIPLE"},
       {"datatype",     'd',     0, 0, "Whether or not to use our own datatype implementation"},
       {"no-flush",     's',     0, 0, "If handed out to the program, do not flush anything until computation has completed."},
       {"prune",        'r',     0, 0, "If handed out to the program, prune the DAG tightly enough (relying on STARPU+mpi cache to avoid redundant data transfers)."},
       {"super-prune",  'R',     0, 0, "If handed out to the program, prune the DAG as tightly as possible."},
       {"prune-handles",'z',     0, 0, "If handed out to the program, prune the handle registration."},
       {"own-context",  'o',     0, 0, "If handed out to the program, schedule tasks in a created context."},
       {"nowarmup",     'w',     0, 0, "If handed out to the program, register warmup run."},
       {"byvalues",     'V',     0, 0, "If handed out to the program, codelet arguments are handed out through STARPU_VALUE instead of cl_gemm_args struct."},
       {"delay",        'D',     0, 0, "If handed out to the program, delay handles registration."},
       {"outer",        'O',     0, 0, "If handed out to the program, submit tasks outer products wise."},
       { 0 }
};


struct arguments
{
  int m, n, k, b;
  int p, q;
  int check,verbose,trace;
  int niter;
  int mpi_thread, datatype;
  int no_flush, prune, super_prune, prune_handles;
  int context;
  int warmup;
  int values;
  int delay;
  int outer;
};


  static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
  {
    case 'm':
      arguments->m = atoi(arg);
      break;
    case 'n':
      arguments->n = atoi(arg);
      break;
    case 'k':
      arguments->k = atoi(arg);
      break;
    case 'b':
      arguments->b = atoi(arg);
      break;
    case 'p':
      arguments->p = atoi(arg);
      break;
    case 'q':
      arguments->q = atoi(arg);
      break;
    case 'c':
      arguments->check = 1;
      break;
    case 'v':
      arguments->verbose = 1;
      break;
    case 'f':
      arguments->trace = 1;
      break;
    case 'l':
      arguments->niter = atoi(arg);
      break;
    case 't':
      arguments->mpi_thread = atoi(arg);
      break;
    case 'd':
      arguments->datatype = 1;
      break;
    case 's':
      arguments->no_flush = 1;
      break;
    case 'r':
      arguments->prune = 1;
      arguments->super_prune = 0;
      break;
    case 'R':
      arguments->super_prune = 1;
      arguments->prune = 0;
      break;
    case 'z':
      arguments->prune_handles = 1;
      break;
    case 'o':
      arguments->context = 1;
      break;
    case 'w':
      arguments->warmup = 0;
      break;
    case 'D':
      arguments->delay = 1;
      break;
    case 'V':
      arguments->values = 1;
      break;
    case 'O':
      arguments->outer = 1;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

static int M  = 1024; /* Matrix size */
static int N  = 1024; /* Matrix size */
static int K  = 1024; /* Matrix size */
static int BS =  512; /* Block size */
static int P  =    1; /* height of the grid */
static int Q  =    1; /* width of the grid */
static int T  =    1; /* number of runs */
static int check = 0;
static int trace = 0; /* whether to trace */
static int datatype = 0; /* whether to register our own datatype */
static int mpi_thread = -1; /* whether to register our own datatype */
static int verbose = 0;
static int flush = 1;
static int prune = 0;
static int super_prune = 0;
static int prune_handles = 0;
static int context = 0;
static int warmup = 1;
static int values = 1;
static int delay = 0;
static int outer = 0;

#define MB ((M)/(BS)) /* Number of blocks */
#define NB ((N)/(BS)) /* Number of blocks */
#define KB ((K)/(BS)) /* Number of blocks */


static int comm_rank; /* mpi rank of the process */
static int comm_size; /* size of the mpi session */

/* Matrices. Will be allocated as regular, linearized C arrays */
static Matrix *A = NULL; /* A will be partitioned as MB x KB blocks */
static Matrix *B = NULL; /* B will be partitioned as KB x NB blocks */
static Matrix *C = NULL; /* C will be partitioned as MB x NB blocks */

starpu_mpi_tag_t tag = 0;

static void alloc_matrices(void)
{
	if (verbose) printf( "[%d] Allocating matrices\n", comm_rank);
	A = alloc_matrix(MB,KB,BS,P,Q,&tag);
	B = alloc_matrix(KB,NB,BS,P,Q,&tag);
	C = alloc_matrix(MB,NB,BS,P,Q,&tag);
}

static void free_matrices(void)
{
	if (verbose) printf( "[%d] Freeing matrices\n", comm_rank);
  	free_matrix(A);
  	free_matrix(B);
  	free_matrix(C);
	if (verbose) printf( "[%d] Freed matrices\n", comm_rank);
}

/* Register the matrix blocks to StarPU and to StarPU-MPI */
static void register_matrices()
{
	if (verbose) printf("[%d] Registering matrices\n", comm_rank);
	if (datatype) {
		starpu_tile_interface_register();
	}

	register_matrix(A,MB,KB,"A",datatype,prune_handles,P,Q,1,0,check,delay);
	register_matrix(B,KB,NB,"B",datatype,prune_handles,P,Q,0,1,check,delay);
	//register_matrix(C,C_h,&tag,MB,NB,datatype,prune_handles,P,Q,0,0); 
	// FIXME (starpu-side) :
	// the previous one seems logical because we do not need to know 
	// about blocks of C we do not contribute to, however startPU seems to be
	// pending on task_insertion if we do not know about blocks on our row/column even if we do not contribute to them)
	// - This could happen because we are seeing a Write on a NULL handle and StarPU is waiting (for what ?)
	register_matrix(C,MB,NB,"C",datatype,prune_handles,P,Q,1,1,check,delay);
}

/* Unregister matrices from the StarPU management. */
static void unregister_matrices()
{
	if (verbose) printf( "[%d] Unregistering matrices\n", comm_rank);
	unregister_matrix(A,MB,KB);
	unregister_matrix(B,KB,NB);
	unregister_matrix(C,MB,NB);
	if (datatype) {
		starpu_tile_interface_register();
	}
	if (verbose) printf( "[%d] Unregistered matrices\n", comm_rank);
}

struct cl_zgemm_args_s {
    double alpha;
    double beta;
};

static void cpu_gemm(void *handles[], void *args)
{

	double *block_A;
	double *block_B;
	double *block_C;
	unsigned n_col_A;
	unsigned n_col_C;
	unsigned n_row_C;
	unsigned ld_A;
	unsigned ld_B;
	unsigned ld_C;
	if (datatype) {
		Block* A = ti_interface_get(handles[0]);
		Block* B = ti_interface_get(handles[1]);
		Block* C = ti_interface_get(handles[2]);
		block_A = A->c;
		block_B = B->c;
		block_C = C->c;
		n_col_A = A->n;
		n_col_C = C->n;
		n_row_C = C->m;
		ld_A = A->ld;
		ld_B = B->ld;
		ld_C = C->ld;
	} else {
	 	block_A = (double *)STARPU_MATRIX_GET_PTR(handles[0]);
	 	block_B = (double *)STARPU_MATRIX_GET_PTR(handles[1]);	
	 	block_C = (double *)STARPU_MATRIX_GET_PTR(handles[2]);	
	 	n_col_A = STARPU_MATRIX_GET_NX(handles[0]);
	 	n_col_C = STARPU_MATRIX_GET_NX(handles[2]);
	 	n_row_C = STARPU_MATRIX_GET_NY(handles[2]);
	 	ld_A = STARPU_MATRIX_GET_LD(handles[0]);
	 	ld_B = STARPU_MATRIX_GET_LD(handles[1]);
	 	ld_C = STARPU_MATRIX_GET_LD(handles[2]);
	}
//	if (verbose) printf("gemm_task\n");
//	printf("DATA %d | ld A %d B %d C %d | alpha %f beta %f \n", datatype, ld_A, ld_B, ld_C, clargs->alpha, clargs->beta);
  double alpha, beta;
  if (values) {
    starpu_codelet_unpack_args(args, &alpha, &beta);
  } else {
    struct cl_zgemm_args_s *clargs = (struct cl_zgemm_args_s *)args;
    alpha=clargs->alpha; beta=clargs->beta;
  }
  double start = starpu_timing_now();
  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, // 2
               n_row_C, n_col_C, n_col_A, alpha, block_A, ld_A, block_B, // 9
               ld_B, beta, block_C, ld_C ); // 13
  double stop = starpu_timing_now();
  double timing = stop - start;
//  printf("gemm_task %f Gflop/s\n", 2.0*n_row_C*n_col_C*n_col_A/(timing*1000));
}

int iseed[4] = { 1,1,1,1 };
static void cpu_fill(void *handles[], void *arg)
{
	(void)arg;
	double *block_A;

	unsigned n_col_A, n_row_A, ld_A;
	if (datatype) {
		Block* tile = ti_interface_get(handles[0]);
		block_A = tile->c;
 		n_col_A = tile->n; 
		n_row_A = tile->m; 
		ld_A    = tile->ld; 
	} else {
 	 	block_A	= (double *)STARPU_MATRIX_GET_PTR(handles[0]);
	 	n_col_A = STARPU_MATRIX_GET_NX(handles[0]);
		n_row_A = STARPU_MATRIX_GET_NY(handles[0]);
		ld_A = STARPU_MATRIX_GET_LD(handles[0]);
	}

	int i,j;
	for (i=0;i<n_row_A;i++)
	{
		LAPACKE_dlarnv(1,iseed,n_col_A,&block_A[i*BS]);
	}
}

static void cpu_copy(void *handles[], void *arg)
{
	(void)arg;
	double *block_A, *block_B;
	unsigned n_col_A, n_row_A;

	if (datatype) {
		Block* tile_A = ti_interface_get(handles[0]);
		Block* tile_B = ti_interface_get(handles[1]);
		block_A = tile_A->c;
		block_B = tile_B->c;
 		n_col_A = tile_A->n; 
		n_row_A = tile_A->m; 
	} else {
		block_A = (double *)STARPU_MATRIX_GET_PTR(handles[0]);
		block_B = (double *)STARPU_MATRIX_GET_PTR(handles[1]);
	 	n_col_A = STARPU_MATRIX_GET_NX(handles[0]);
		n_row_A = STARPU_MATRIX_GET_NY(handles[0]);
	}

	int i,j;
	//if (verbose) printf("fill_task\n");
	for (i = 0; i < n_row_A; i++) {
		for (j = 0; j < n_col_A; j++) {
			block_A[i*n_col_A + j] = block_B[i*n_col_A + j];
		}
	}
}

static void cpu_nrm2_comp(void *handles[], void *arg)
{
//	printf("nrm task\n");
	(void)arg;
	// FIXME
	double *block_A = (double *)STARPU_MATRIX_GET_PTR(handles[0]);
	double *block_B = (double *)STARPU_MATRIX_GET_PTR(handles[1]);

	unsigned n_col_A, n_row_A;
	if (datatype) {
		Block* tile = ti_interface_get(handles[0]);
 		n_col_A = tile->n; 
		n_row_A = tile->m; 
	} else {
	 	n_col_A = STARPU_MATRIX_GET_NX(handles[0]);
		n_row_A = STARPU_MATRIX_GET_NY(handles[0]);
	}

	int i,j;
	//if (verbose) printf("fill_task\n");
	double local_ssq = 0.0;
	double tmp;
	for (i = 0; i < n_row_A; i++) {
		for (j = 0; j < n_col_A; j++) {
			tmp = (block_A[i*n_col_A + j] - block_B[i*n_col_A + j]);
			local_ssq += tmp*tmp;
		}
	}
	i = n_row_A - 1;
	j = n_col_A - 1;
	printf("Some block diff : %f (%d,%d: %f - %f)\n", local_ssq, i,j, block_A[i*n_col_A+j], block_B[i*n_col_A+j]);
	//printf("Some block diff : %f \n", local_ssq);
}

/* Define a StarPU 'codelet' structure for the matrix multiply kernel above.
 * This structure enable specifying multiple implementations for the kernel (such as CUDA or OpenCL versions)
 */
static struct starpu_codelet gemm_cl =
{
	.cpu_funcs = {cpu_gemm}, /* cpu implementation(s) of the routine */
	.nbuffers = 3, /* number of data handles referenced by this routine */
	.modes = {STARPU_R, STARPU_R, STARPU_RW}, /* access modes for each data handle */
	.name = "gemm" /* to display task name in traces */
};

static struct starpu_codelet fill_cl =
{
	.cpu_funcs = {cpu_fill}, /* cpu implementation(s) of the routine */
	.nbuffers = 1, /* number of data handles referenced by this routine */
	.modes = {STARPU_W},
	.name = "fill" /* to display task name in traces */
};

static struct starpu_codelet copy_cl =
{
	.cpu_funcs = {cpu_copy}, /* cpu implementation(s) of the routine */
	.nbuffers = 2, /* number of data handles referenced by this routine */
	.modes = {STARPU_W,STARPU_R},
	.name = "copy" /* to display task name in traces */
};

// TODO : redux ssq 
static struct starpu_codelet nrm_cl =
{
	.cpu_funcs = {cpu_nrm2_comp}, /* cpu implementation(s) of the routine */
	.nbuffers = 2, /* number of data handles referenced by this routine */
	.modes = {STARPU_R,STARPU_R},
	.name = "nrm2_comp" /* to display task name in traces */
};

static void init_matrix(Matrix* X, int mb, int nb)
{
	int row, col;
	for (row = 0; row < mb; row++)
	{
		for (col = 0; col < nb; col++)
		{
			if (X->blocks[row*nb+col].owner == comm_rank)
			{
//				printf("[%d] fill X_%d,%d %p\n",comm_rank,row,col, X->blocks[row*nb+col].hdl);
				starpu_mpi_task_insert(MPI_COMM_WORLD, &fill_cl,
					STARPU_W, X->blocks[row*nb+col].hdl, 0);
//				printf("[%d] filled X_%d,%d\n",comm_rank,row,col);
			}
		}
	}
}

static void copy_matrix(Matrix* A, Matrix* B)
{
	int row, col;
	for (row = 0; row < A->mb; row++)
	{
		for (col = 0; col < A->nb; col++)
		{
			if (A->blocks[row*A->nb+col].owner == comm_rank
			 || B->blocks[row*A->nb+col].owner == comm_rank)
			{
				starpu_mpi_task_insert(MPI_COMM_WORLD, &copy_cl,
					STARPU_W, A->blocks[row*A->nb+col].hdl,
					STARPU_R, B->blocks[row*A->nb+col].hdl, 0);
			}
		}
	}
	starpu_mpi_wait_for_all(MPI_COMM_WORLD);
}

static void init_matrices(void)
{
	if (verbose) printf( "[%d] Initializing matrices\n", comm_rank);
	// I own all the blocks
	init_matrix(A,MB,KB);
	starpu_mpi_wait_for_all(MPI_COMM_WORLD);
	init_matrix(B,KB,NB);
	starpu_mpi_wait_for_all(MPI_COMM_WORLD);
	init_matrix(C,MB,NB);
	starpu_mpi_wait_for_all(MPI_COMM_WORLD);
	if (verbose) printf( "[%d] Initialized matrices\n", comm_rank);
}

void submit_gemm(int b_row, int b_col, int b_aisle, double alpha,double beta,unsigned ctx) {
	double zbeta;
	int a_local, b_local, c_local;
	Block *Ail, *Blj, *Cij;
	Ail = & A->blocks[b_row*KB + b_aisle];
	Blj = & B->blocks[b_aisle*NB + b_col];
	Cij = & C->blocks[b_row * NB + b_col];
	a_local = Ail->owner == comm_rank;	
	b_local = Blj->owner == comm_rank;	
	c_local = Cij->owner == comm_rank;	
	// when prune and/or prune_handles are allowed needs to be clarified
	//if ((!prune && !prune_handles) || (A->blocks[b_row*KB+b_aisle].owner == comm_rank || B->blocks[b_aisle*NB+b_col].owner == comm_rank || C->blocks[b_row*NB+b_col].owner == comm_rank)) { 
	// TODO : logic might be written more clearly (a/b/c_local may be redundant)
	if ((!super_prune || (c_local || (a_local && b_col <= Q) || (b_local && b_row <= P) )) && 
	    (!prune || (a_local || b_local || c_local))) {
		if (delay) {
//			printf("[%d] late registration i,j,l %d,%d,%d\n",comm_rank,b_row,b_col,b_aisle);
			if (!prune_handles || c_local) {
				block_starpu_register(Ail,datatype);
				block_starpu_register(Blj,datatype);
			}
			block_starpu_register(Cij, datatype);
		}
		struct cl_zgemm_args_s *clargs = NULL;
		if (c_local) {
			if (verbose) printf("[%d] exec. C_%d,%d (%d-%p-%d) += A_%d,%d (%d-%p-%d) B_%d,%d (%d-%p-%d)\n", comm_rank,
				b_row,b_col,  Cij->registered,Cij->hdl,Cij->owner,
				b_row,b_aisle,Ail->registered,Ail->hdl,Ail->owner,
				b_aisle,b_col,Blj->registered,Blj->hdl,Blj->owner);
			if (!values) {
				clargs = malloc(sizeof( struct cl_zgemm_args_s ));
				clargs->alpha = alpha;
				clargs->beta = b_aisle==0? beta : 1.0;
			}
		} else if (verbose){
			printf("[%d] insert. C_%d,%d (%d-%p-%d) += A_%d,%d (%d-%p-%d) B_%d,%d (%d-%p-%d)\n", comm_rank,
				b_row,b_col,  Cij->registered,Cij->hdl,Cij->owner,
				b_row,b_aisle,Ail->registered,Ail->hdl,Ail->owner,
				b_aisle,b_col,Blj->registered,Blj->hdl,Blj->owner);
		}
 		if (context) {
			starpu_mpi_task_insert(MPI_COMM_WORLD, &gemm_cl,
				STARPU_CL_ARGS, clargs, sizeof(struct cl_zgemm_args_s), 
				STARPU_SCHED_CTX, ctx,
				STARPU_R, Ail->hdl,
				STARPU_R, Blj->hdl,
				STARPU_RW,Cij->hdl, 0);
		} else if (values) {
			zbeta = b_aisle == 0 ? beta : 1.0;
			starpu_mpi_task_insert(MPI_COMM_WORLD, &gemm_cl,
				STARPU_VALUE, &alpha, sizeof(double),
				STARPU_VALUE, &zbeta, sizeof(double),
				STARPU_R, Ail->hdl,
				STARPU_R, Blj->hdl,
				STARPU_RW,Cij->hdl, 0);
		} else {
			starpu_mpi_task_insert(MPI_COMM_WORLD, &gemm_cl,
				STARPU_CL_ARGS, clargs, sizeof(struct cl_zgemm_args_s), 
				STARPU_R, Ail->hdl,
				STARPU_R, Blj->hdl,
				STARPU_RW,Cij->hdl, 0);
		}
	} else {
//		printf("[%d] NOT inserted C_%d,%d += A_%d,%d B_%d,%d\n",comm_rank, b_row,b_col, b_row,b_aisle, b_aisle,b_col);
	}
}

int main(int argc, char *argv[])
{
	struct arguments arguments;
	/* Default values */
	arguments.m = M;
	arguments.n = N;
	arguments.k = K;
	arguments.b = BS;
	arguments.p = P;
	arguments.q = Q;
	arguments.check = 0;
	arguments.verbose = 0;
	arguments.trace = 0;
	arguments.niter = T;
	arguments.mpi_thread = mpi_thread;
	arguments.datatype = datatype;
	arguments.no_flush = 0;
  	arguments.prune = 0;
  	arguments.super_prune = 0;
  	arguments.prune_handles = 0;
 	arguments.context = 0;
	arguments.delay = 0;
	arguments.outer = 0;
	arguments.warmup = 1;
	arguments.values = 0;
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	M = arguments.m;
	N = arguments.n;
	K = arguments.k;
	BS = arguments.b;
	P = arguments.p;
	Q = arguments.q;
	check = arguments.check;
	verbose = arguments.verbose;
	trace = arguments.trace;
	T = arguments.niter;
	mpi_thread = arguments.mpi_thread;
	datatype = arguments.datatype;
	flush = !arguments.no_flush;
  	prune = arguments.prune;
  	super_prune = arguments.super_prune;
  	prune_handles = arguments.prune_handles;
 	context = arguments.context;
	delay = arguments.delay;
	outer = arguments.outer;
	warmup = arguments.warmup;
	values = arguments.values;

	/* Initializes StarPU and the StarPU-MPI layer */
	starpu_fxt_autostart_profiling(0);
	int provided_mpi_thread, ret;
	if (mpi_thread == -1) {
		ret = starpu_mpi_init_conf(&argc, &argv, 1, MPI_COMM_WORLD, NULL);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_ini_conft");
	} else {
		switch(mpi_thread) {
    case 0:
      MPI_Init_thread(NULL,NULL,MPI_THREAD_SINGLE,&provided_mpi_thread);
      break;
    case 1:
      MPI_Init_thread(NULL,NULL,MPI_THREAD_FUNNELED,&provided_mpi_thread);
      break;
    case 2:
      MPI_Init_thread(NULL,NULL,MPI_THREAD_SERIALIZED,&provided_mpi_thread);
      break;
    case 3:
      MPI_Init_thread(NULL,NULL,MPI_THREAD_MULTIPLE,&provided_mpi_thread);
      break;
		}
		ret = starpu_mpi_init_conf(&argc, &argv, 0, MPI_COMM_WORLD, NULL);
	}

	/* Get the process rank and session size */
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &comm_size);

	if (comm_rank == 0) printf("Launching with %d arguments\n",argc);

	int ncpu = starpu_cpu_worker_get_count();
	if (ncpu == 0)
	{
		fprintf(stderr, "We need at least 1 CPU worker.\n");
		starpu_mpi_shutdown();
		return (comm_rank == 0) ? STARPU_TEST_SKIPPED : 0;
	}
	if (P < 1 || Q < 1 || P*Q != comm_size)
	{
		fprintf(stderr, "invalid grid size\n");
		starpu_mpi_shutdown();
		return (comm_rank == 0) ? 1 : 0;
	}
	if (BS < 1 || M % BS != 0 || N % BS != 0 || K % BS != 0)
	{
		if (comm_rank == 0) fprintf(stderr, "invalid block size\n");
		starpu_mpi_shutdown();
		return (comm_rank == 0) ? 1 : 0;
	}
	if (comm_rank == 0)
	{
		printf("MxNxK  = %dx%dx%d\n", M, N, K);
		printf("BS     = %d\n", BS);
		printf("MxNxKb = %dx%dx%d\n", MB,NB,KB);
		printf("comm_size = %d\n", comm_size);
		printf("PxQ = %dx%d\n", P, Q);
		if (trace) 		printf("- Tracing enabled\n");
		if (check) 		printf("- Checking enabled\n");
		if (datatype) 		printf("- MPI datatype enabled\n");
		if (mpi_thread > -1) 	printf("- MPI thread support level : %d\n", provided_mpi_thread);
		if (!flush) 		printf("- Flushing disabled\n");
		if (super_prune) 	printf("- Super-Pruning enabled\n");
		if (prune) 		printf("- Pruning enabled\n");
		if (prune_handles) 	printf("- Handle pruning enabled\n");
		if (context)		printf("- Submitting own context\n");
		if (!warmup)		printf("- Warmup disabled\n");
		if (values)		printf("- Passing by values (instead of cl_args)\n");
		if (delay)		printf("- Delayed handle registration enabled\n");
        if (outer)      printf("- Submit tasks by outer products");
    }
  	int barrier_ret, trial;
     	double start, stop;
	double alpha = 3.14, beta=0.42;
	barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
	if (trace) starpu_fxt_start_profiling();
	int* procs;
	unsigned ctx;
 	if (context) {
		procs = (int*) malloc(ncpu*sizeof(int));
		starpu_worker_get_ids_by_type(STARPU_CPU_WORKER, procs, ncpu);
		ctx = starpu_sched_ctx_create(procs, ncpu,"std_ctx",
			STARPU_SCHED_CTX_POLICY_NAME, "lws", 0);
	}
	if (warmup) T++;
	for (trial =0; trial < T; trial++)
	{
	        alloc_matrices();
		register_matrices();
	        init_matrices();

		Matrix* Cwork;
		if (check) {
			Cwork = alloc_matrix(MB,NB,BS,1,1,&tag);
			register_matrix(Cwork,MB,NB,"Cwork",datatype,0,1,1,1,1,1,0);
			copy_matrix(Cwork,C);
			starpu_mpi_wait_for_all(MPI_COMM_WORLD);
			if (verbose) print_matrix(C,"Cinit");
			if (verbose) print_matrix(Cwork,"Cwork");
		}
// 		starpu_data_display_memory_stats();
	        barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
		start = starpu_timing_now();

		int b_row,b_col,b_aisle;
	    Block *Ail, *Blj, *Cij;
        if (outer) {
  			for (b_aisle=0;b_aisle<KB;b_aisle++)
   			{
         		for (b_row = 0; b_row < MB; b_row++)
         		{
         			for (b_col = 0; b_col < NB; b_col++)
         			{
                        submit_gemm(b_row,b_col,b_aisle,alpha,beta, ctx);
    				}
    			}
    			if (flush) {
         		    for (b_row = 0; b_row < MB; b_row++) {   
    					Ail = & A->blocks[b_row*KB + b_aisle];
    					if (Ail->registered) starpu_mpi_cache_flush(MPI_COMM_WORLD, Ail->hdl);
                    }
         			for (b_col = 0; b_col < NB; b_col++) {
    					Blj = & B->blocks[b_aisle*NB + b_col];
    					if (Blj->registered) starpu_mpi_cache_flush(MPI_COMM_WORLD, Blj->hdl);
    				}
    			}
    		}
        } else {
    		for (b_row = 0; b_row < MB; b_row++)
    		{
    			for (b_col = 0; b_col < NB; b_col++)
    			{
    				for (b_aisle=0;b_aisle<KB;b_aisle++)
    				{
                        submit_gemm(b_row,b_col,b_aisle,alpha,beta,ctx);
    				}
    			}
    			if (flush) {
    				for (b_aisle=0;b_aisle<KB;b_aisle++) { 
    					Ail = & A->blocks[b_row*KB + b_aisle];
    					if (Ail->registered) starpu_mpi_cache_flush(MPI_COMM_WORLD, Ail->hdl);
    				}
    			}
    		}
    	}
//		printf("[%d] finished submission\n",comm_rank);
		starpu_mpi_wait_for_all(MPI_COMM_WORLD);
	        barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
		stop = starpu_timing_now();
		double timing = stop - start;
		if (comm_rank==0 && (!warmup || trial > 0)) printf("RANK %d -> took %f s | %f Gflop/s\n", comm_rank, timing/1000/1000, 2.0*M*N*K/(timing*1000));

		if (check) {
			Matrix* Acheck, *Bcheck, *Ccheck;

			Acheck = alloc_matrix(MB,KB,BS,1,1,&tag);
			Bcheck = alloc_matrix(KB,NB,BS,1,1,&tag);
			Ccheck = alloc_matrix(MB,NB,BS,1,1,&tag);

			register_matrix(Acheck,MB,KB,"Acheck",datatype,0,1,1,1,1,1,0);
			register_matrix(Bcheck,KB,NB,"Bcheck",datatype,0,1,1,1,1,1,0);
			register_matrix(Ccheck,MB,NB,"Check",datatype,0,1,1,1,1,1,0);

			copy_matrix(Acheck,A);
			if (verbose) print_matrix(A,"A");
			if (verbose) print_matrix(Acheck,"Ac");
			copy_matrix(Bcheck,B);
			if (verbose) print_matrix(B,"B");
			if (verbose) print_matrix(Bcheck,"Bc");
			copy_matrix(Ccheck,C);
			if (verbose) print_matrix(C,"C");
			if (verbose) print_matrix(Ccheck,"Cc");
			if (comm_rank == 0) {
				int b_row,b_col,b_aisle;
				for (b_row = 0; b_row < MB; b_row++) {
					for (b_col = 0; b_col < NB; b_col++) {
						for (b_aisle=0;b_aisle<KB;b_aisle++) {
								struct cl_zgemm_args_s *clargs = NULL;
								clargs = malloc(sizeof( struct cl_zgemm_args_s ));
								clargs->alpha = alpha;
								clargs->beta = b_aisle==0? beta : 1.0;
								starpu_mpi_task_insert(MPI_COMM_WORLD, &gemm_cl,
									STARPU_CL_ARGS, clargs, sizeof(struct cl_zgemm_args_s), 
									STARPU_R,  Acheck->blocks[b_row*KB+b_aisle].hdl,
									STARPU_R,  Bcheck->blocks[b_aisle*NB+b_col].hdl,
									STARPU_RW, Cwork->blocks[b_row*NB+b_col].hdl,  0);
						}
						starpu_mpi_task_insert(MPI_COMM_WORLD, &nrm_cl,
							STARPU_R, Ccheck->blocks[b_row*NB+b_col].hdl,
							STARPU_R, Cwork->blocks[b_row*NB+b_col].hdl,  0);
					}
				}
				starpu_mpi_wait_for_all(MPI_COMM_WORLD);
			} 
	       	 	barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
	       	 	unregister_matrix(Acheck,MB,KB);
	       	 	unregister_matrix(Bcheck,KB,NB);
	       	 	unregister_matrix(Ccheck,MB,NB);
	       	 	unregister_matrix(Cwork,MB,NB);
	       	 	barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
  	       	 	free_matrix(Acheck);
  	       	 	free_matrix(Bcheck);
  	       	 	free_matrix(Ccheck);
  	       	 	free_matrix(Cwork);
	       		barrier_ret = starpu_mpi_barrier(MPI_COMM_WORLD);
		}
		starpu_mpi_cache_flush_all_data(MPI_COMM_WORLD);
		unregister_matrices();
		free_matrices();
	}

	if (context) {
		starpu_sched_ctx_delete(ctx);
		free(procs);
	}
	if (trace) starpu_fxt_stop_profiling();
	starpu_mpi_shutdown();
	if (mpi_thread > -1) MPI_Finalize();
	return 0;
}
