#ifndef DENSE_MAT_FNCT_H
#define DENSE_MAT_FNCT_H

typedef struct Blocks
{
        double* c; 
        int m,n,ld;
        int owner;
	//starpu_data_handle_t* hdl;
	starpu_data_handle_t hdl;
	starpu_mpi_tag_t tag;
	int registered;
} Block;

typedef struct Matrices
{       
        int mb, nb, b;
        Block* blocks;
} Matrix;

Matrix* alloc_matrix(int mb, int nb, int b, int p, int q, starpu_mpi_tag_t * tag);
void free_matrix(Matrix* X);
void register_matrix(Matrix* X, int mb, int nb, char* name, int datatype, int prune_handles, int p, int q, int row, int col, int check, int delay);
void unregister_matrix(Matrix* X, int mb, int nb);
void print_matrix(Matrix* X, char* name);
void block_starpu_register(Block* Xij, int datatype);

#endif
